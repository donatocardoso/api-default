# Veja aqui também:

- [API Node + MongoDB](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MONGODB.md)
- [API Node + MSSQL](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MSSQL.md)

# Read Me For Development With SEQUELIZE

<img src="images/api-icon.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/nodejs-logo.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/sequelize-logo.png" width="150" heigth="150" />

Nesse repositório está presente uma API funcional que faz conexão ao MongoDB e Sql Server (atráves do MSSQL e Sequelize).

Aqui falaremos sobre a implementação Node.js que faz conexão com Sql Server utilizando a biblioteca _**SEQUELIZE**_, que por sua vez seguiu a seguinte estrutura:

## Estrutura

```javascript
    |
    |_ src
    |   |_ 1.controllers
    |   |   |_ sequelizeController.js
    |   |_ 2.services
    |   |   |_ dtos
    |   |   |   |_ sequelizeDto.js
    |   |   |_ sequelizeService.js
    |   |_ 3.repositories
    |   |   |_ sequelizeRepository.js
    |   |_ 4.connections
    |   |   |_ sequelizeConnection.js
    |		|- customs.js
    |		|- returnDto.js
    |		|- server.js
    |_ app.js
    |_ package-lock.json
    |_ package.json
```

## Explicando a Estrutura

### Arquivos:
- **_customs.js:** Arquivo reponsável por fazer override nos metódos com erro que pertence a uma biblioteca presente na node_modules. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_custom.js)

- **_parameters.js:** Arquivo que contém as variáveis globais que serão acessadas em todo projeto, aconselha-se colocar aqui os caminhos de servidores, usuários e senhas de acesso. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_parameters.js)

- **_returnModel.js:** Model para mensagem padrão de retornos da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_returnModel.js)

- **_routes.js:** Arquivo que chama as rotas presentes nos controllers para acesso as funções da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_routes.js)

- **app.js:** Arquivo de criação e configuração do servidor que será rodado. (_Obs: Somente esse arquivo em toda API segue as normas do ECMASCRIPT5_). [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/app.js)

Pastas:
----

### 1.controllers/

Nesse diretório está presente a classe **BaseController** _(responsável por salvar os logs)_ e os demais _**controllers**_ que contém as rotas e suas respectivas funções que as aplicações vão chamar e assim fazer uso da API.  _**Obs:**_ É obrigatório fazer a herança da classe **BaseController** em todos os _controllers_.

Os _**controllers**_ devem ser iniciados e exportados da seguinte forma:

```javascript
import BaseController from "./_baseController";

export default class SequelizeController extends BaseController
{
	constructor()
	{
		super();
	}
}
```

Após sua criação basta adicionar o método _**routes()**_ para criação das rotas das demais funções. É importante fazer uso da função _**super.log()**_ para chamar as funções presente nas classes filhas de **BaseController** para registrar os logs no _**Database**_.

É de responsabilidade dos _**controllers**_ verificar se os parâmetros solicitados pelas funções foram devidamente informados, seja por _query string_ ou pelo _body_ da requisição. [Clique aqui e veja o exemplo completo](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/1.controllers/sqlServerController.js).

### 2.services/

Nesse diretório está presente as _**models/**_ utilizadas em toda API, além dos _**services**_ que são responsáveis por verificar, tratar e manipular os dados que serão recebidos e devolvidos na _**request**_.

As _**models/**_ contém os objetos que serão verificados, tratados e manipulados pelas _**services**_. É obrigatório a implementação das funções _**defaultProps()**_, _**postIsValid()**_ e _**putIsValid()**_ em todas as _**models/**_ para que a regra de verificação dos dados fique somente em um lugar na API e assim facilite sua manutenção. Veja os exemplos de _**model**_ e _**service**_:

````javascript
import { DataTypes } from "sequelize";

import ReturnModel from "../../../_returnModel";

export default class SequelizeModel
{
	constructor(model = {})
	{
		this.id = model.id;
		this.var_method = model.var_method;
		this.var_url = model.var_url;
		this.var_param = model.var_param;
		this.var_erro = model.var_erro;
		this.date = model.date;
	}

	defaultProps()
	{
		return {
			id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true, unique: true },
			var_method: { type: DataTypes.STRING(4000), allowNull: false },
			var_url: { type: DataTypes.STRING(4000), allowNull: false },
			var_param: { type: DataTypes.STRING(4000), allowNull: false },
			var_erro: { type: DataTypes.STRING(4000), allowNull: true },
			date: { type: DataTypes.DATE, defaultValue: DataTypes.NOW }
		};
	}

	async postIsValid()
	{
		if (!this.var_method) return new ReturnModel(201, "Parâmetro 'sequelizeModel.var_method' não encontrado");
		if (!this.var_url) return new ReturnModel(201, "Parâmetro 'sequelizeModel.var_url' não encontrado");
		if (!this.var_param) return new ReturnModel(201, "Parâmetro 'sequelizeModel.var_param' não encontrado");

		return new ReturnModel(200, "OK", this);
	}
	
	async putIsValid()
	{
		if (!this.id) return new ReturnModel(201, "Parâmetro 'sequelizeModel.id' não encontrado");
		if (!this.var_erro) return new ReturnModel(201, "Parâmetro 'sequelizeModel.var_erro' não encontrado");

		return new ReturnModel(200, "OK", this);
	}
}
````

````javascript
import ReturnModel from "../../_returnModel";

import SequelizeModel from "./models/sequelizeModel";
import SequelizeRepository from "../../3.repositories/sequelizeRepository";

export default class SequelizeService
{
	async getAll()
	{
		return await new SequelizeRepository().getAll();
	}

	...

	async addNew(obj)
	{
		let retorno = await new SequelizeModel(obj).postIsValid();

		if (retorno.statusCode == 201) return retorno;

		return await new SequelizeRepository().addNew(retorno.content);
  }

  ...
}
````

### 3.repositories/

Nesse diretório está presente os _**repositories**_ utilizadas para fazer conexão com o _**Database**_, é obrigatório fazer a herança da classe de conexão pressente em _**4.connections/sequelizeConnection.js**_. 

Os _**repositories**_ possuem a responsabilidade de conetar no _**Database**_ e tratar seus retornos, isso fazendo uso da função implementada pela _**connection**_ (**super.modelConnection()**). Veja:

````javascript
import ReturnModel from "../_returnModel";

import SequelizeModel from "../2.services/sequelize/models/sequelizeModel";
import SequelizeConnection from '../4.connections/sequelizeConnection'

export default class SequelizeRepository extends SequelizeConnection
{
  constructor()
	{
		super("MYDATABASE", "tb_api_score_venda_log", new SequelizeModel().defaultProps());
	}

	async getAll()
	{
		let model = await super.modelConnection();

		return new ReturnModel(200, "OK", await model.findAll({
			attributes: ["id", "var_method", "var_url", "var_param", "var_erro", "date"],
			order: [["id", "DESC"]]
		}));
	}

	...

	async addNew(obj = {})
	{
		let model = await super.modelConnection();
		
		let retorno = await model.create({
			var_method: obj.var_method,
			var_url: obj.var_url,
			var_param: obj.var_param
		});

		return retorno.dataValues
			? new ReturnModel(200, "OK", retorno.dataValues)
			: new ReturnModel(201, "Falha na alteração dos dados");
	}
	 
	...
}
````

### 4.connections/

Nesse diretório está presente a _**connection**_ que utiliza a biblioteca **SEQUELIZE**, essa classe já está configurada para conexão e criação da model de conexão com o _**Database**_. Veja:

````javascript
import Sequelize from "sequelize";

import Parameters from "../_parameters";

export default class SequelizeConnection
{
  constructor(databaseName = "", tableName = "", model = {})
  {
    this._tableName = tableName;
    this._model = model;

    this._conn = new Sequelize(databaseName, Parameters.usuarioServidorDB, Parameters.senhaServidorDB, {
      host: Parameters.caminhoServidorDBTeste,
      dialect: "mssql",
      logging: false
    });
  }

  async modelConnection()
  {
    await this._conn.sync();

    return await this._conn.define(this._tableName, this._model,
      {
        freezeTableName: true,
        sequelize: Sequelize,
        timestamps: false
      });
  }
}
````

Exemplo de Chamada e Retorno da API
----

<img src="images/chamada-e-retorno-sequelize.png" />
