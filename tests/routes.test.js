import supertest from "supertest";
import Server from "../src/server";

Server.setConfigs();

describe("START MOCHA SCRIPT TEST", function ()
{
  this.timeout(10000);

  describe("MongoController", function ()
  {
    let idRecord = null;
    
    it(`POST ${process.env.BASE_PATH}/mongodb`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .post(`${process.env.BASE_PATH}/mongodb`)
          .timeout(10000)
          .send({
            mongoDbDto: {
              var_descricao: "MOCHA SCRIPT TEST - POST TO 'mongodbController'"
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode == 200)
            {
              idRecord = res.body.content._id;
            }
            else
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/mongodb/1`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/mongodb/1`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/mongodb/1/idRecord`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/mongodb/1/${idRecord}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`PUT ${process.env.BASE_PATH}/mongodb`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .put(`${process.env.BASE_PATH}/mongodb`)
          .send({
            mongoDbDto: {
              id: idRecord,
              var_descricao: "MOCHA SCRIPT TEST - PUT"
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
      
    it(`DELETE ${process.env.BASE_PATH}/mongodb`, function ()
    {
      return new Promise((resolve, reject) => 
      {
        supertest(Server.getServerApp(true))
          .delete(`${process.env.BASE_PATH}/mongodb`)
          .send({
            mongoDbDto: {
              id: idRecord
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
  });

  describe("SequelizeController", function ()
  {
    let idRecord = null;

    it(`POST ${process.env.BASE_PATH}/sequelize`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .post(`${process.env.BASE_PATH}/sequelize`)
          .timeout(10000)
          .send({
            sequelizeDto: {
              var_descricao: "MOCHA SCRIPT TEST - POST TO 'SequelizeController'"
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode == 200)
            {
              idRecord = res.body.content.id;
            }
            else
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/sequelize/1`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/sequelize/1`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/sequelize/1/idRecord`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/sequelize/1/${idRecord}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`PUT ${process.env.BASE_PATH}/sequelize`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .put(`${process.env.BASE_PATH}/sequelize`)
          .send({
            sequelizeDto: {
              id: idRecord,
              var_descricao: "MOCHA SCRIPT TEST - PUT",
              flg_ativo: false
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
      
    it(`DELETE ${process.env.BASE_PATH}/sequelize`, function ()
    {
      return new Promise((resolve, reject) => 
      {
        supertest(Server.getServerApp(true))
          .delete(`${process.env.BASE_PATH}/sequelize`)
          .send({
            sequelizeDto: {
              id: idRecord
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
  });

  describe("SqlServerController", function ()
  {
    let idRecord = null;

    it(`POST ${process.env.BASE_PATH}/sqlServer`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .post(`${process.env.BASE_PATH}/sqlServer`)
          .timeout(10000)
          .send({
            sqlServerDto: {
              var_descricao: "MOCHA SCRIPT TEST - POST TO 'SqlServerController'"
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode == 200)
            {
              idRecord = res.body.content.id;
            }
            else
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/sqlServer/1`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/sqlServer/1`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`GET ${process.env.BASE_PATH}/sqlServer/1/idRecord`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .get(`${process.env.BASE_PATH}/sqlServer/1/${idRecord}`)
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });

    it(`PUT ${process.env.BASE_PATH}/sqlServer`, function ()
    {
      return new Promise((resolve, reject) =>
      {
        supertest(Server.getServerApp(true))
          .put(`${process.env.BASE_PATH}/sqlServer`)
          .send({
            sqlServerDto: {
              id: idRecord,
              var_descricao: "MOCHA SCRIPT TEST - PUT"
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
      
    it(`DELETE ${process.env.BASE_PATH}/sqlServer`, function ()
    {
      return new Promise((resolve, reject) => 
      {
        supertest(Server.getServerApp(true))
          .delete(`${process.env.BASE_PATH}/sqlServer`)
          .send({
            sqlServerDto: {
              id: idRecord
            }
          })
          .set("Accept", "application/json")
          .expect("Content-Type", /json/)
          .expect(200)
          .end((err, res) =>
          {
            err ? reject(err) : resolve();

            if (res.body.statusCode != 200)
              console.log("         • " + res.body.message);
          });
      });
    });
  });
});
