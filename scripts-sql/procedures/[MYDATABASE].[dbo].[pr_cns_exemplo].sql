USE [MYDATABASE]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[pr_cns_exemplo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[pr_cns_exemplo]
GO

CREATE PROCEDURE [dbo].[pr_cns_exemplo]
	@page	INT = NULL,
	@id		INT = NULL

	AS
		
	/*	======================================================================
	AUTHOR.......: Donato Cardoso
	CREATE DATE..: 05/06/2019
	DESCRIPTION..: Exibi um exemplo
		
	EXAMPLE......: EXEC [MYDATABASE].[dbo].[pr_cns_exemplo] @id = NULL
	====================================================================== */

	BEGIN
		
		SELECT	id,
				var_descricao,
				flg_ativo
			FROM [MYDATABASE].[dbo].[tb_exemplo]
			WHERE flg_ativo = 1
				AND (id = @id OR @id IS NULL)

	END
GO
