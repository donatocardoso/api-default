USE [MYDATABASE]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[pr_upd_exemplo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[pr_upd_exemplo]
GO

CREATE PROCEDURE [dbo].[pr_upd_exemplo]
	@id				INT,
	@var_descricao	VARCHAR(100)

	AS
		
	/*	======================================================================
	AUTHOR.......:	Donato Cardoso
	CREATE DATE..:	05/06/2019
	DESCRIPTION..:	Exibi um exemplo
		
	EXAMPLE......:	DECLARE @retorno INT;
					EXEC @retorno = [MYDATABASE].[dbo].[pr_upd_exemplo]
						@id				= 3,
						@var_descricao	= 'Teste Update';
					SELECT @retorno AS retorno;

	RETURNS......:	0 - OK
					1 - O registro n�o foi encontrado
					2 - Falha ao alterar o registro
	====================================================================== */

	BEGIN
		
		BEGIN TRANSACTION;

			IF NOT EXISTS (SELECT TOP 1 1
							FROM [MYDATABASE].[dbo].[tb_exemplo]
							WHERE id = @id
								and flg_ativo = 1)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 1;
				END

			UPDATE [MYDATABASE].[dbo].[tb_exemplo]
				SET var_descricao = @var_descricao
				WHERE id = @id;

			IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 2;
				END

		COMMIT TRANSACTION;
		RETURN 0;

	END
GO
