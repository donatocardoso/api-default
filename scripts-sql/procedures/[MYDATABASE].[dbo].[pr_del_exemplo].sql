USE [MYDATABASE]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[pr_del_exemplo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[pr_del_exemplo]
GO

CREATE PROCEDURE [dbo].[pr_del_exemplo]
	@id INT

	AS
		
	/*	======================================================================
	AUTHOR.......:	Donato Cardoso
	CREATE DATE..:	05/06/2019
	DESCRIPTION..:	Exibi um exemplo
		
	EXAMPLE......:	DECLARE @retorno INT;
					EXEC @retorno = [MYDATABASE].[dbo].[pr_del_exemplo] @id = 2
					SELECT @retorno AS retorno;

	RETURNS......:	0 - OK
					1 - O registro n�o foi encontrado
					2 - Falha ao deletar o registro
	====================================================================== */

	BEGIN
		
		BEGIN TRANSACTION;

			IF NOT EXISTS (SELECT TOP 1 1
							FROM [MYDATABASE].[dbo].[tb_exemplo]
							WHERE id = @id)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 1;
				END

			UPDATE [MYDATABASE].[dbo].[tb_exemplo]
				SET flg_ativo = 0
				WHERE id = @id;

			IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 2;
				END

		COMMIT TRANSACTION;
		RETURN 0;

	END
GO
