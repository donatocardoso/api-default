USE [MYDATABASE]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[pr_ins_exemplo]') AND objectproperty(id, N'IsPROCEDURE')=1)
	DROP PROCEDURE [dbo].[pr_ins_exemplo]
GO

CREATE PROCEDURE [dbo].[pr_ins_exemplo]
	@var_descricao VARCHAR(100)

	AS
		
	/*	======================================================================
	AUTHOR.......:	Donato Cardoso
	CREATE DATE..:	05/06/2019
	DESCRIPTION..:	Exibi um exemplo
		
	EXAMPLE......:	DECLARE @retorno INT;
					EXEC @retorno = [MYDATABASE].[dbo].[pr_ins_exemplo] 
						@var_descricao = 'Teste';
					SELECT @retorno AS retorno;

	RETURNS......:	0 - OK
					1 - J� existe um registro com essa descri��o
					2 - Falha ao salvar o registro
	====================================================================== */

	BEGIN
		
		BEGIN TRANSACTION;

			IF EXISTS (SELECT TOP 1 1
						FROM [MYDATABASE].[dbo].[tb_exemplo]
						WHERE var_descricao = @var_descricao)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 1;
				END

			INSERT INTO [MYDATABASE].[dbo].[tb_exemplo]
				(var_descricao)
					VALUES
						(@var_descricao);

			IF (@@ERROR <> 0 OR @@ROWCOUNT = 0)
				BEGIN
					ROLLBACK TRANSACTION;
					RETURN 2;
				END

		COMMIT TRANSACTION;
		RETURN (SCOPE_IDENTITY() * -1);

	END
GO
