USE [MYDATABASE]
GO

CREATE TABLE [dbo].[tb_exemplo] (
	id				INT				NOT NULL	IDENTITY(1,1)	,
	var_descricao	VARCHAR(100)	NOT NULL					,
	flg_ativo		BIT				NOT NULL	DEFAULT 1		,

	CONSTRAINT pk_tb_exemplo_id PRIMARY KEY (id)
);
