import color from "cli-color";
import express from "express";
import swaggerUi from "swagger-ui-express";

const serverError = ((req, res, callback) => 
{
  callback(req, res)
    .then((response) =>
    {
      return res
        .status(response.statusCode)
        .json(response);
    })
    .catch((error) =>
    {
      let msg = `Ocorreu um erro ao processar os dados. TypeError: ${error.message}`;
    
      let stack = error.stack.split("\n");

      return res.status(400).json({
        statusCode: 400,
        message: msg,
        content: stack
      });
    });
});

const serverApp = (function ()
{
  return express();
})();
  
const serverRouter = (function ()
{
  const router = express.Router();

  router.route("/").get(function (req, res)
  {
    return res.status(200).json({
      statusCode: 200,
      message: `${serverProps.name.toUpperCase()}: OK! - process.env.NODE_ENV: ${process.env.NODE_ENV}`
    });
  });

  return router;
})();

const baseRoute = {
  method: "",
  path: "",
  tags: [""],
  summary: "",
  parameters: {},
  responses: {},
  handler: function () { }
}

const swaggerProps = {
  layout: {
    explorer: true,
    customfavIcon: "",
    customCss: "",
    customSiteTitle: ""
  },
  specification: {
    openapi: "3.0.0",
    info: {
      name: "",
      title: "",
      version: "",
      description: ""
    },
    servers: [
      {
        url: ""
      }
    ],
    components: {},
    paths: {}
  }
};

export default class ExpressSwagger
{
  static getServerApp(fullApp = false)
  {
    if (fullApp)
    {
      const routeDocs = process.env.BASE_PATH + "/docs";
      const swaggerSetup = swaggerUi.setup(swaggerProps.specification, swaggerProps.layout);

      serverApp.use(process.env.BASE_PATH, serverRouter);
      serverApp.use(routeDocs, swaggerUi.serve, swaggerSetup);
    }

    return serverApp;
  }

  static addRoute(route = baseRoute) 
  {
    let method = serverRouter.route(route.path)[route.method.toLowerCase()](function (req, res)
    {
      serverError(req, res, route.handler);
    });

    if (swaggerProps.specification.paths[route.path])
    {
      Object.assign(swaggerProps.specification.paths[route.path], {
        [method]: {
          tags: route.tags,
          summary: route.summary,
          parameters: route.parameters,
          responses: route.responses
        }
      });
    }
    else
    {
      swaggerProps.specification.paths[route.path] = {
        [method]: {
          tags: route.tags,
          summary: route.summary,
          parameters: route.parameters,
          responses: route.responses
        }
      };
    }
  }

  static setSwaggerProps(props = swaggerProps)
  {
    Object.assign(swaggerProps.layout, props.layout);
    Object.assign(swaggerProps.specification, props.specification);
  }

  static listen()
  {
    this.getServerApp(true)
      .listen(process.env.PORT, () =>   
      {
        var _name = `\n ${swaggerProps.specification.info.name.toUpperCase()} `;
        var _description = `\n ${swaggerProps.specification.info.description} `;

        var _environment = `\n process.env.NODE_ENV: ${process.env.ENVIRONMENT || process.env.NODE_ENV} `;
        var _baseRoute = `\n Base Route: http://${process.env.HOST}:${process.env.PORT}${process.env.BASE_PATH} `;
        var _docsRoute = `\n Docs Route: http://${process.env.HOST}:${process.env.PORT}${process.env.BASE_PATH}/docs `;

        var message = `\n${_name} ${_description} ${_environment} ${_baseRoute} ${_docsRoute}`;

        console.log(color.bgWhite(color.black(message.replace(/([^\s][^\n]{1,50})/g, "$1 \n"))));
        console.log("");
      });
  }
}
