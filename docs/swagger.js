import { name, version, description } from "../package.json";

export const layout = {
  explorer: false,
  customfavIcon: "https://www.consorciomagalu.com.br/images/consorcio-icone.ico",
  customCss: ".swagger-ui .topbar { display: none }",
  customSiteTitle: "Documentação da Api Default"
};

export const specification = {
  info: {
    name: name,
    version: version,
    description: description,
    title: "Documentação da Api Default"
  },
  server: [
    {
      url: `http://${process.env.HOST}:${process.env.PORT}${process.env.BASE_PATH}`
    }
  ],
  components:{
    securitySchemes: {
      authorization: {
        type: "apiKey",
        name: "authorization",
        in: "header"
      },
      filial: {
        type: "apiKey",
        name: "filial",
        in: "header"
      },
      chapa: {
        type: "apiKey",
        name: "chapa",
        in: "header"
      }
    },
    schemas: {
      ReturnDto: {
        type: "object",
        properties: {
          statusCode: {
            type: "integer",
            format: "int64",
            description: "Código de Estado da requisição"
          },
          message: {
            type: "string",
            description: "Mensagem de Estado da requisição"
          },
          content: {
            type: "object",
            description: "Conteúdo de retorno da requisição, caso haja"
          }
        }
      }
    },
    responses: {
      default: {
        description: "Retorno padrão da Api",
        content: {
          "application/json": {
            schema: {
              $ref: "#/components/schemas/ReturnDto"
            }
          }
        }
      }
    }
  }
};
