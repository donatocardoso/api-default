# Api Default

Nessa Api está desenvolvido três exemplos de conexão ao _**Database**_, sendo _**MongoDB, MSSQL**_ e _**Sequelize**_.

Escolha a opção que deseja acessar:

- [API Node + MongoDB](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MONGODB.md)
- [API Node + MSSQL](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MSSQL.md)
- [API Node + Sequelize](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_SEQUELIZE.md)

Desenvolvido por Donato C. Ávila, com o intuito de padronizar o desenvolvimento de API's no Consórcio Luiza. Para maiores detalhes entrar em contato através do email donato.avila@consorcioluiza.com.br

## Startup

- 1) Utilize os comandos:
  - **Produção**: npm start
  - **Desenvolvimento**: npm run dev

- **Obs**: Utilize o comando _npm test_ para executar os testes com o Mochajs

## License

MIT
