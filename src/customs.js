import * as DataTypes from "sequelize/lib/data-types";

Date.prototype.toFormatedSequelize = function ()
{
  let now = new Date(this);

  let year = now.getFullYear();
  let month = ("0" + (now.getMonth() + 1)).slice(-2);
  let day = ("0" + now.getDate()).slice(-2);
  
  let hour = ("0" + (now.getHours() - 3)).slice(-2);
  let minutes = ("0" + now.getMinutes()).slice(-2);
  let seconds = ("0" + now.getSeconds()).slice(-2);
  let miliseconds = ("0" + now.getMilliseconds()).slice(-3);
  
  let date = `${year}-${month}-${day} ${hour}:${minutes}:${seconds}.${miliseconds}`;

  return date;
};

DataTypes.DATE.prototype._stringify = function _stringify(date, options)
{
  date = this._applyTimezone(date, options);
  
  // Z here means current timezone, _not_ UTC
  // return date.format("YYYY-MM-DD HH:mm:ss.SSS Z");
  return date.format("YYYY-MM-DD HH:mm:ss.SSS");
};
