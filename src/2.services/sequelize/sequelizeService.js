import ReturnDto from "../../returnDto";

import SequelizeDto from "./dtos/sequelizeDto";
import SequelizeRepository from "../../3.repositories/sequelizeRepository";

export default class SequelizeService
{
	async getAll(page)
	{
		return await new SequelizeRepository().get(page);
	}

	async get(page, id)
	{
		if (isNaN(parseInt(id)))
			return new ReturnDto(400, "Tipo do parametro 'id' deve ser 'number'");

		return await new SequelizeRepository().get(page, { id: parseInt(id) });
	}

	async addNew(obj)
	{
		let retorno = await new SequelizeDto(obj).postIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new SequelizeRepository().addNew(retorno.content);
	}

	async update(obj)
	{
		let retorno = await new SequelizeDto(obj).putIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new SequelizeRepository().update(retorno.content, {
			id: retorno.content.id
		});
	}

	async delete(id)
	{
		if (isNaN(parseInt(id)))
			return new ReturnDto(400, "Tipo do parametro 'id' deve ser 'number'");

		return await new SequelizeRepository().delete({ id: parseInt(id) });
	}
}
