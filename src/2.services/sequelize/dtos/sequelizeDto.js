import { DataTypes } from "sequelize";

import ReturnDto from "../../../returnDto";

export default class SequelizeDto
{
	constructor(dto = {})
	{
		this.id = dto.id;
		this.var_descricao = dto.var_descricao;
		this.flg_ativo = dto.flg_ativo;
	}

	defaultProps()
	{
		return {
			id: { type: DataTypes.BIGINT, primaryKey: true, autoIncrement: true, unique: true },
			var_descricao: { type: DataTypes.STRING(4000), allowNull: false },
			flg_ativo: { type: DataTypes.BOOLEAN, allowNull: true, defaultProps: true }
		};
	}

	async postIsValid()
	{
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'sequelizeDto.var_descricao' não encontrado");

		return new ReturnDto(200, "OK", this);
	}
	
	async putIsValid()
	{
		if (!this.id) return new ReturnDto(400, "Parâmetro 'sequelizeDto.id' não encontrado");
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'sequelizeDto.var_descricao' não encontrado");

		return new ReturnDto(200, "OK", this);
	}
}

