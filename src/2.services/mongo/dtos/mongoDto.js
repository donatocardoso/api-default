import Mongoose from "mongoose";

import ReturnDto from "../../../returnDto";

export default class MongoDto
{
	constructor(dto = {})
	{
		this.id = dto.id;
		this.var_descricao = dto.var_descricao;
		this.flg_ativo = dto.flg_ativo;
	}

	defaultProps()
	{
		return new Mongoose.Schema({
			var_descricao: { type: String, required: "var_descricao is required" },
			flg_ativo: { type: Boolean, required: "flg_ativo is required", default: true }
		});
	}

	postIsValid()
	{
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'mongoDto.var_descricao' não encontrado");

		return new ReturnDto(200, "OK", this);
	}

	putIsValid()
	{
		if (!this.id) return new ReturnDto(400, "Parâmetro 'mongoDto.id' não encontrado");
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'mongoDto.var_descricao' não encontrado");

		return new ReturnDto(200, "OK", this);
	}
}
