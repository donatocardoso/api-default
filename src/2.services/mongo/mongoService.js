import ReturnDto from "../../returnDto";

import MongoDto from "./dtos/mongoDto";
import MongoRepository from "../../3.repositories/mongoRepository";

export default class MongoService
{
	async getAll(page)
	{
		return await new MongoRepository().get({
			page: page,
			id: null
		});
	}

	async get(page, id)
	{
		return await new MongoRepository().get({
			page: null,
			id: id
		});
	}

	async addNew(obj)
	{
		let retorno = new MongoDto(obj).postIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new MongoRepository().addNew(retorno.content);
	}

	async update(obj)
	{
		let retorno = new MongoDto(obj).putIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new MongoRepository().update(retorno.content);
	}

	async delete(obj)
	{
		if (typeof(obj.id) != "string")
			return new ReturnDto(400, "Tipo do parametro 'obj.id' deve ser 'string'");

		return await new MongoRepository().delete(obj);
	}
}
