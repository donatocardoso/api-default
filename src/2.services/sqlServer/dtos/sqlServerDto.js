import ReturnDto from "../../../returnDto";

export default class SqlServerDto
{
	constructor(dto = {})
	{
		this.id = dto.id;
		this.var_descricao = dto.var_descricao;
		this.flg_ativo = dto.flg_ativo;
	}

	postIsValid()
	{
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'sqlServerDto.var_descricao' não encontrado");

		return new ReturnDto(200, "OK", this);
	}

	putIsValid()
	{
		if (!this.id) return new ReturnDto(400, "Parâmetro 'sqlServerDto.id' não encontrado");
		if (!this.var_descricao) return new ReturnDto(400, "Parâmetro 'sqlServerDto.var_descricao' não encontrado");
		
		return new ReturnDto(200, "OK", this);
	}
}
