import ReturnDto from "../../returnDto";

import SqlServerDto from "./dtos/sqlServerDto";
import SqlServerRepository from "../../3.repositories/sqlServerRepository";

export default class SqlServerService
{
	async getAll(page)
	{
		return await new SqlServerRepository().get(page, null);
	}

	async get(page, id)
	{
		return await new SqlServerRepository().get(page, id);
	}

	async addNew(obj)
	{
		let retorno = await new SqlServerDto(obj).postIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new SqlServerRepository().addNew(retorno.content);
	}

	async update(obj)
	{
		let retorno = await new SqlServerDto(obj).putIsValid();

		if (retorno.statusCode == 400) return retorno;

		return await new SqlServerRepository().update(retorno.content);
	}

	async delete(id)
	{
		if (isNaN(parseInt(id)))
			return new ReturnDto(400, "Tipo do parametro 'id' deve ser 'number'");

		return await new SqlServerRepository().delete(parseInt(id));
	}
}
