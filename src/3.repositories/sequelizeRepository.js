import ReturnDto from "../returnDto";

import SequelizeDto from "../2.services/sequelize/dtos/sequelizeDto";
import SequelizeConnection from '../4.connections/sequelizeConnection'

export default class SequelizeRepository extends SequelizeConnection
{
	constructor()
	{
		super("MYDATABASE", "tb_exemplo", new SequelizeDto().defaultProps());
	}

	async get(page, objWhere = {})
	{
		let dto = await super.dtoConnection();

		return new ReturnDto(200, "OK", await dto.findAll({
			attributes: [
				"id",
				"var_descricao",
				"flg_ativo"
			],
			where: objWhere,
			offset: (page - 1) * 25,
			limit: 25
		}));
	}

	async addNew(obj = {})
	{
		let dto = await super.dtoConnection();
		
		let retorno = await dto.create({
			var_descricao: obj.var_descricao,
			flg_ativo: obj.flg_ativo
		});

		return retorno.dataValues
			? new ReturnDto(200, "OK", retorno.dataValues)
			: new ReturnDto(400, "Falha na alteração dos dados");
	}

	async update(obj = {}, objWhere = {})
	{
		let dto = await super.dtoConnection();

		let retorno = await dto.update({
			var_descricao: obj.var_descricao,
			flg_ativo: obj.flg_ativo
		},
		{
			where: objWhere
		});

		if (!retorno[0])
			new ReturnDto(400, "Falha na alteração dos dados");
		
		return await this.get(objWhere);
	}

	async delete(objWhere = {})
	{
		let dto = await super.dtoConnection();
		
		let retorno = await dto.destroy({ where: objWhere });

		return retorno
			? new ReturnDto(200, "OK")
			: new ReturnDto(400, "Falha na alteração dos dados");
	}
}
