import MongoDto from "../2.services/mongo/dtos/mongoDto";
import MongoConnection from "../4.connections/mongoConnection";

export default class MongoRepository extends MongoConnection
{
	constructor()
	{
		super("MYDATABASE", "exemplos", new MongoDto().defaultProps());
	}

	async get(obj)
	{
		if (obj.page)
		{
			let exemplos = await super.findAll();

			let start = (obj.page - 1) * 25;
			let end = ((obj.page - 1) * 25) + 25;

			exemplos.content = exemplos.content.slice(start, end);

			return exemplos;
		}
		else
			return await super.findOne({
				_id: obj.id
			});
	}

	async addNew(dto)
	{
		return await super.save({
			var_descricao: dto.var_descricao,
		});
	}

	async update(dto)
	{
		let retorno = await super.update(
			{
				_id: dto.id
			},
			{
				var_descricao: dto.var_descricao,
			}
		);

		if (retorno.statusCode != 200) return retorno;

		return super.findOne({
			_id: dto.id
		});
	}

	async delete(obj)
	{
		return await super.delete({
			_id: obj.id
		});
	}
}
