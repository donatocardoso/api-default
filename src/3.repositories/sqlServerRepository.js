import ReturnDto from "../returnDto";

import SqlServerConnection from "../4.connections/sqlServerConnection";

export default class SqlServerRepository extends SqlServerConnection
{
	constructor()
	{
		super();
	}

	async get(page, id)
	{
		return await super.query("MYDATABASE", "[dbo].[pr_cns_exemplo]", {
			page: page,
			id: id
		});
	}

	async addNew(dto)
	{
		let retorno = await super.non_query("MYDATABASE", "[dbo].[pr_ins_exemplo]", {
			var_descricao: dto.var_descricao
		});

		if (retorno.statusCode != 200)
			return retorno;

		if (retorno.content < 0)
			return new ReturnDto(200, "OK", {
				id: (parseInt(retorno.content) * -1)
			});
		
		switch (retorno.content)
		{
			case 1: return new ReturnDto(400, "Já existe um registro com essa descrição");
			case 2: return new ReturnDto(400, "Falha ao salvar o registro");
			default: return new ReturnDto(400, "Falha ao verificar retorno do banco");
		}
	}

	async update(dto)
	{
		let retorno = await super.non_query("MYDATABASE", "[dbo].[pr_upd_exemplo]", {
			id: dto.id,
			var_descricao: dto.var_descricao,
		});

		if (retorno.statusCode != 200)
			return retorno;

		switch (retorno.content)
		{
			case 0: return new ReturnDto(200, "OK");
			case 1: return new ReturnDto(400, "O registro não foi encontrado");
			case 2: return new ReturnDto(400, "Falha ao alterar o registro");
			default: return new ReturnDto(400, "Falha ao verificar retorno do banco");
		}
	}

	async delete(id)
	{
		let retorno = await super.non_query("MYDATABASE", "[dbo].[pr_del_exemplo]", {
			id: id
		});

		if (retorno.statusCode != 200)
			return retorno;

		switch (retorno.content)
		{
			case 0: return new ReturnDto(200, "OK");
			case 1: return new ReturnDto(400, "O registro não foi encontrado");
			case 2: return new ReturnDto(400, "Falha ao deletar o registro");
			default: return new ReturnDto(400, "Falha ao verificar retorno do banco");
		}
	}
}
