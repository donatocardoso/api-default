import "./customs";

import cors from "cors";
import dotenv from "dotenv";
import bodyParser from "body-parser";

import ExpressSwagger from "../docs/expressSwagger";
import { specification, layout } from "../docs/swagger";

import MongoController from "./1.controllers/mongoController";
import SequelizeController from "./1.controllers/sequelizeController";
import SqlServerController from "./1.controllers/sqlServerController";
		
export default class Server extends ExpressSwagger
{
	static setConfigs()
	{
		dotenv.config({
			path: ((process.env.NODE_ENV + "").trim() == "production"
				? "./.env-production"
				: "./.env-homolog")
		});

		ExpressSwagger.setSwaggerProps({ layout: layout, specification: specification });

		ExpressSwagger.getServerApp().use(cors());
		ExpressSwagger.getServerApp().use(bodyParser.urlencoded({ extended: true }));
		ExpressSwagger.getServerApp().use(bodyParser.json());
		
		MongoController.setRoutes();
		SequelizeController.setRoutes();
		SqlServerController.setRoutes();
	}
}
