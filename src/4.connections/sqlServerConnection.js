import sql from "mssql";

import ReturnDto from "../returnDto";

export default class SqlServerConnection
{
	constructor() { }
	
	async pool(databaseName = "")
	{
		return new sql.ConnectionPool({
			user: process.env.DB_USER,
			password: process.env.DB_PASSWORD,
			server: process.env.DB_SERVER,
			database: databaseName,
			options: {
				encrypt: true,
			},
		});
	}

	async query(databaseName = "", query = "", params = {})
	{
		if (typeof query == "string" && typeof params == "object")
		{
			try
			{
				return new Promise((resolve, reject) => {
					this.pool(databaseName)
						.then(pool => {
							pool
								.connect()
								.then(pool => {
									let request = new sql.Request(pool);

									for (let prop in params)
										request.input(
											prop,
											this.typeParam(params[prop]),
											params[prop],
										);

									request
										.execute(query)
										.then(result => {
											resolve(new ReturnDto(200, "OK", result["recordset"]));
										})
										.catch(err =>
											reject(new ReturnDto(400, "Falha", err.toString())),
										);
								})
								.catch(error =>
									reject(new ReturnDto(400, "Falha", error.toString())),
								);
						})
						.catch(error =>
							reject(new ReturnDto(400, "Falha", error.toString())),
						);
				});
			}
			catch (error)
			{
				return new ReturnDto(400, error.toString());
			}
		}
	}

	async non_query(databaseName = "", query = "", params = {})
	{
		if (typeof query == "string" && typeof params == "object")
		{
			try
			{
				return new Promise((resolve, reject) => {
					this.pool(databaseName)
						.then(pool => {
							pool
								.connect()
								.then(pool => {
									let request = new sql.Request(pool);

									for (let prop in params)
										request.input(
											prop,
											this.typeParam(params[prop]),
											params[prop],
										);

									request
										.execute(query)
										.then(result =>
										{
											resolve(new ReturnDto(200, "OK", result.returnValue));
										})
										.catch(err =>
											reject(new ReturnDto(400, "Falha", err.toString())),
										);
								})
								.catch(error =>
									reject(new ReturnDto(400, "Falha", error.toString())),
								);
						})
						.catch(error =>
							reject(new ReturnDto(400, "Falha", error.toString())),
						);
				});
			}
			catch (error)
			{
				return new ReturnDto(400, error.toString());
			}
		}
	}

	typeParam(param)
	{
		switch (typeof param)
		{
			case "string": return sql.VarChar(param.length);
			case "number": return sql.Int;
			case "boolean": return sql.Bit;
			default: return sql.VarChar;
		}
	}
}
