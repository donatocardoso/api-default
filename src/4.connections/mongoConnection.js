import Mongoose from "mongoose";

import ReturnDto from "../returnDto";

export default class MongoConnection
{
	constructor(nameDatabase = "MYDATABASE", nameSchema = "exemplos", schema = {})
	{
		this._schema = schema;
		this._nameSchema = nameSchema;
		this._nameDatabase = nameDatabase;
	}

	async connectDb()
	{
		return await Mongoose.connect(`${process.env.MONGO_CONNECTION}/${this._nameDatabase}`,
			{
				useNewUrlParser: true,
			},
		);
	}

	async findAll()
	{
		await this.connectDb();

		let retorno = await Mongoose
			.model(this._nameSchema, this._schema)
			.find()
			.then(result => {
				return new ReturnDto(200, "OK", result);
			})
			.catch(error => {
				return new ReturnDto(400, error.toString());
			});

		delete Mongoose.connection.models[this._nameSchema];

		return retorno;
	}

	async findOne(obj = {})
	{
		await this.connectDb();

		let retorno = await Mongoose
			.model(this._nameSchema, this._schema)
			.findOne(obj)
			.then(result => {
				return new ReturnDto(200, "OK", result || {});
			})
			.catch(error => {
				return new ReturnDto(400, error.toString());
			});

		delete Mongoose.connection.models[this._nameSchema];

		return retorno;
	}

	async save(obj = {})
	{
		await this.connectDb();

		let schema = await Mongoose.model(this._nameSchema, this._schema);

		let retorno = await new schema(obj)
			.save()
			.then(result => {
				return new ReturnDto(200, "OK", result);
			})
			.catch(error => {
				return new ReturnDto(400, error.toString());
			});

		delete Mongoose.connection.models[this._nameSchema];

		return retorno;
	}

	async update(cond = {}, Dto = {})
	{
		await this.connectDb();

		let retorno = await Mongoose
			.model(this._nameSchema, this._schema)
			.updateOne(cond, Dto)
			.then(result => {
				return new ReturnDto(200, "OK");
			})
			.catch(error => {
				return new ReturnDto(400, error.toString());
			});

		delete Mongoose.connection.models[this._nameSchema];

		return retorno;
	}

	async delete(dto = {})
	{
		await this.connectDb();

		let retorno = await Mongoose
			.model(this._nameSchema, this._schema)
			.deleteOne(dto)
			.then(result => {
				return new ReturnDto(200, "OK");
			})
			.catch(error => {
				return new ReturnDto(400, error.toString());
			});

		delete Mongoose.connection.models[this._nameSchema];

		return retorno;
	}
}
