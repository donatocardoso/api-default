import Sequelize from "sequelize";

export default class SequelizeConnection
{
  constructor(databaseName = "", tableName = "", model = {})
  {
    this._tableName = tableName;
    this._model = model;

    this._conn = new Sequelize(databaseName, process.env.DB_USER, process.env.DB_PASSWORD, {
      host: process.env.DB_SERVER,
      dialect: "mssql",
      logging: false
    });
  }

  async dtoConnection()
  {
    await this._conn.sync();

    return await this._conn.define(this._tableName, this._model,
      {
        freezeTableName: true,
        sequelize: Sequelize,
        timestamps: false
      });
  }
}
