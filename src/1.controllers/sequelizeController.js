import ReturnDto from "../returnDto";
import ExpressSwagger from "../../docs/expressSwagger";

import SequelizeService from "../2.services/sequelize/sequelizeService";

export default class SequelizeController
{
	static setRoutes()
	{
		ExpressSwagger.addRoute({
			method: "GET",
			path: "/sequelize/:page",
			tags: ["Sequelize"],
			summary: "Retorna todos os registros da tabela 'MYDATABASE.dbo.tb_exemplo' do banco utilizando o Sequelize",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto().upd(400, "Parâmetro 'page' não encontrado!");
				
				return await new SequelizeService().getAll(req.params.page);
			}
		});

		ExpressSwagger.addRoute({
			method: "GET",
			path: "/sequelize/:page/:id",
			tags: ["Sequelize"],
			summary: "Retorna um registro específico da tabela 'MYDATABASE.dbo.tb_exemplo' do banco utilizando o Sequelize",
			parameters: [
				{
					name: "id",
					in: "path",
					required: true,
					description: "Id do exemplo",
					schema: {
						type: "integer",
						format: "int64",
						minimum: 1
					}
				}
			],
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto().upd(400, "Parâmetro 'page' não encontrado!");
				
				if (!req.params.id)
					return new ReturnDto().upd(400, "Parâmetro 'id' não encontrado!");

				return await new SequelizeService().get(req.params.page, req.params.id);
			}
		});

		ExpressSwagger.addRoute({
			method: "POST",
			path: "/sequelize",
			tags: ["Sequelize"],
			summary: "Inseri um registro na tabela 'MYDATABASE.dbo.tb_exemplo' do banco utilizando o Sequelize",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sequelizeDto)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto' não encontrado!");

				if (!req.body.sequelizeDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto.var_descricao' não encontrado!");
				
				return await new SequelizeService().addNew(req.body.sequelizeDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "PUT",
			path: "/sequelize",
			tags: ["Sequelize"],
			summary: "Altera um registro na tabela 'MYDATABASE.dbo.tb_exemplo' do banco utilizando o Sequelize",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sequelizeDto)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto' não encontrado!");

				if (!req.body.sequelizeDto.id)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto.id' não encontrado!");
				
				if (!req.body.sequelizeDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto.var_descricao' não encontrado!");
				
				return await new SequelizeService().update(req.body.sequelizeDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "DELETE",
			path: "/sequelize",
			tags: ["Sequelize"],
			summary: "Deleta um registro na tabela 'MYDATABASE.dbo.tb_exemplo' do banco utilizando o Sequelize",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sequelizeDto)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto' não encontrado!");

				if (!req.body.sequelizeDto.id)
					return new ReturnDto(400, "Parâmetro 'sequelizeDto.id' não encontrado!");

				return await new SequelizeService().delete(req.body.sequelizeDto.id);
			}
		});
	}
}
