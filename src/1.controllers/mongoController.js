import ReturnDto from "../returnDto";
import ExpressSwagger from "../../docs/expressSwagger";

import MongoService from "../2.services/mongo/mongoService";

export default class MongoController
{
	static setRoutes()
	{
		ExpressSwagger.addRoute({
			method: "GET",
			path: "/mongodb/:page",
			tags: ["MongoDB"],
			summary: "Retorna todos os registros da collection 'MYDATABASE.exemplos' do MongoDB",
			responses: {
				200: {
					description: "Chamada executada com sucesso.",
					$ref: "#/components/responses/default"
				},
				400: {
					description: "Chamada executada com erros.",
					$ref: "#/components/responses/default"
				},
				401: {
					description: "Inautorizado! Verifique os headers.",
					$ref: "#/components/responses/default"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto(400, "Parâmetro 'page' não encontrado!");
				
				return await new MongoService().getAll(req.params.page);
			}
		});

		ExpressSwagger.addRoute({
			method: "GET",
			path: "/mongodb/:page/:id",
			tags: ["MongoDB"],
			summary: "Retorna um registro específico da collection 'MYDATABASE.exemplos' do MongoDB",
			parameters: [
				{
					name: "id",
					in: "query",
					required: true,
					description: "Id do exemplo",
					schema: {
						type: "integer",
						format: "int64",
						minimum: 1,
						example: 1
					}
				}
			],
			responses: {
				200: {
					description: "Chamada executada com sucesso",
					$ref: "#/components/responses/default"
				},
				400: {
					description: "Chamada executada com erros",
					$ref: "#/components/responses/default"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto(400, "Parâmetro 'page' não encontrado!");
				
				if (!req.params.id)
					return new ReturnDto(400, "Parâmetro 'id' não encontrado!");

				return await new MongoService().get(req.params.page, req.params.id);
			}
		});

		ExpressSwagger.addRoute({
			method: "POST",
			path: "/mongodb",
			tags: ["MongoDB"],
			summary: "Inseri um novo registro na collection 'MYDATABASE.exemplos' do MongoDB",
			responses: {
				200: {
					description: "Chamada executada com sucesso",
					$ref: "#/components/responses/default"
				},
				400: {
					description: "Chamada executada com erros",
					$ref: "#/components/responses/default"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.mongoDbDto)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto' não encontrado!");

				if (!req.body.mongoDbDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto.var_descricao' não encontrado!");
				
				return await new MongoService().addNew(req.body.mongoDbDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "PUT",
			path: "/mongodb",
			tags: ["MongoDB"],
			summary: "Altera um registro na collection 'MYDATABASE.exemplos' do MongoDB",
			responses: {
				200: {
					description: "Chamada executada com sucesso",
					$ref: "#/components/responses/default"
				},
				400: {
					description: "Chamada executada com erros",
					$ref: "#/components/responses/default"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.mongoDbDto)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto' não encontrado!");

				if (!req.body.mongoDbDto.id)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto.id' não encontrado!");
				
				if (!req.body.mongoDbDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto.var_descricao' não encontrado!");
				
				return await new MongoService().update(req.body.mongoDbDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "DELETE",
			path: "/mongodb",
			tags: ["MongoDB"],
			summary: "Delete um registro na collection 'MYDATABASE.exemplos' do MongoDB",
			responses: {
				200: {
					description: "Chamada executada com sucesso",
					$ref: "#/components/responses/default"
				},
				400: {
					description: "Chamada executada com erros",
					$ref: "#/components/responses/default"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.mongoDbDto)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto' não encontrado!");

				if (!req.body.mongoDbDto.id)
					return new ReturnDto(400, "Parâmetro 'mongoDbDto.id' não encontrado!");
				
				return await new MongoService().delete(req.body.mongoDbDto);
			}
		});
	}
}
