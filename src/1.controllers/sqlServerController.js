import ReturnDto from "../returnDto";
import ExpressSwagger from "../../docs/expressSwagger";

import SqlServerService from "../2.services/sqlServer/sqlServerService";

export default class SqlServerController
{
	static setRoutes()
	{
		ExpressSwagger.addRoute({
			method: "GET",
			path: "/sqlserver/:page",
			tags: ["SqlServer"],
			summary: "Retorna todos os registros do banco utilizando o MSSQL para executar procedures",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto(400, "Parâmetro 'page' não encontrado!");
				
				return await new SqlServerService().getAll();
			}
		});

		ExpressSwagger.addRoute({
			method: "GET",
			path: "/sqlserver/:page/:id",
			tags: ["SqlServer"],
			summary: "Retorna todos os registros do banco utilizando o MSSQL para executar procedures",
			parameters: [
				{
					name: "id",
					in: "path",
					required: true,
					description: "Id do exemplo",
					schema: {
						type: "integer",
						format: "int64",
						minimum: 1
					}
				}
			],
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.params.page)
					return new ReturnDto(400, "Parâmetro 'page' não encontrado!");
				
				if (!req.params.id)
					return new ReturnDto(400, "Parâmetro 'id' não encontrado!");

				return await new SqlServerService().get(req.params.id);
			}
		});

		ExpressSwagger.addRoute({
			method: "POST",
			path: "/sqlserver",
			tags: ["SqlServer"],
			summary: "Inseri um registro no banco utilizando o MSSQL para executar procedures",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sqlServerDto)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto' não encontrado!");

				if (!req.body.sqlServerDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto.var_descricao' não encontrado!");
				
				return await new SqlServerService().addNew(req.body.sqlServerDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "PUT",
			path: "/sqlserver",
			tags: ["SqlServer"],
			summary: "Altera um registro no banco utilizando o MSSQL para executar procedures",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sqlServerDto)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto' não encontrado!");

				if (!req.body.sqlServerDto.id)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto.id' não encontrado!");
				
				if (!req.body.sqlServerDto.var_descricao)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto.var_descricao' não encontrado!");
				
				return await new SqlServerService().update(req.body.sqlServerDto);
			}
		});

		ExpressSwagger.addRoute({
			method: "DELETE",
			path: "/sqlserver",
			tags: ["SqlServer"],
			summary: "Deleta um registro no banco utilizando o MSSQL para executar procedures",
			responses: {
				200: {
					description: "Chamada executada com sucesso"
				},
				400: {
					description: "Chamada executada com erros"
				}
			},
			handler: async (req) =>
			{
				if (!req.body.sqlServerDto)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto' não encontrado!");

				if (!req.body.sqlServerDto.id)
					return new ReturnDto(400, "Parâmetro 'sqlServerDto.id' não encontrado!");
				
				return await new SqlServerService().delete(req.body.sqlServerDto.id);
			}
		});
	}
}
