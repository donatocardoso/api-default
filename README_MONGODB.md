# Veja aqui também:

- [API Node + MSSQL](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MSSQL.md)
- [API Node + Sequelize](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_SEQUELIZE.md)

# Read Me For Development With MONGODB

<img src="images/api-icon.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/nodejs-logo.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/mongo-logo.png" width="150" heigth="150" />

Nesse repositório está presente uma API funcional que faz conexão ao MongoDB e Sql Server (atráves do MSSQL e Sequelize).

Aqui falaremos sobre a implementação Node.js que faz conexão com Sql Server utilizando a biblioteca _**MONGODB**_, que por sua vez seguiu a seguinte estrutura:

## Estrutura

```javascript
    |
    |_ src
    |   |_ 1.controllers
    |   |   |_ mongoController.js
    |   |_ 2.services
    |   |   |_ dtos
    |   |   |   |_ mongoDto.js
    |   |   |_ mongoService.js
    |   |_ 3.repositories
    |   |   |_ mongoRepository.js
    |   |_ 4.connections
    |   |   |_ mongoConnection.js
    |		|- customs.js
    |		|- returnDto.js
    |		|- server.js
    |_ app.js
    |_ package-lock.json
    |_ package.json
```

## Explicando a Estrutura

### Arquivos:
- **_customs.js:** Arquivo reponsável por fazer override nos metódos com erro que pertence a uma biblioteca presente na node_modules. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_custom.js)

- **_parameters.js:** Arquivo que contém as variáveis globais que serão acessadas em todo projeto, aconselha-se colocar aqui os caminhos de servidores, usuários e senhas de acesso. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_parameters.js)

- **_returnModel.js:** Model para mensagem padrão de retornos da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_returnModel.js)

- **_routes.js:** Arquivo que chama as rotas presentes nos controllers para acesso as funções da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_routes.js)

- **app.js:** Arquivo de criação e configuração do servidor que será rodado. (_Obs: Somente esse arquivo em toda API segue as normas do ECMASCRIPT5_). [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/app.js)

Pastas:
----

### 1.controllers/

Nesse diretório está presente a classe **BaseController** _(responsável por salvar os logs)_ e os demais _**controllers**_ que contém as rotas e suas respectivas funções que as aplicações vão chamar e assim fazer uso da API.  _**Obs:**_ É obrigatório fazer a herança da classe **BaseController** em todos os _controllers_.

Os _**controllers**_ devem ser iniciados e exportados da seguinte forma:

```javascript
import BaseController from "./_baseController";

export default class MongoController extends BaseController
{
	constructor()
	{
		super();
	}
}
```

Após sua criação basta adicionar o método _**routes()**_ para criação das rotas das demais funções. É importante fazer uso da função _**super.log()**_ para chamar as funções presente nas classes filhas de **BaseController** para registrar os logs no _**Database**_.

É de responsabilidade dos _**controllers**_ verificar se os parâmetros solicitados pelas funções foram devidamente informados, seja por _query string_ ou pelo _body_ da requisição. [Clique aqui e veja o exemplo completo](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/1.controllers/sqlServerController.js).

### 2.services/

Nesse diretório está presente as _**models/**_ e _**schemas/**_ utilizadas em toda API, além dos _**services**_ que são responsáveis por verificar, tratar e manipular os dados que serão recebidos e devolvidos na _**request**_.

As _**models/**_ contém os objetos que serão verificados, tratados e manipulados pelas _**services**_. É obrigatório a implementação das funções _**defaultProps()**_, _**postIsValid()**_ e _**putIsValid()**_ em todas as _**models/**_ para que a regra de verificação dos dados fique somente em um lugar na API e assim facilite sua manutenção. Veja os exemplos de _**model**_ e _**service**_:

````javascript
import Mongoose from "mongoose";

import ReturnModel from "../../../_returnModel";

export default class MongoModel
{
	constructor(model = {})
	{
		this._id = model._id;
		this.fileName = model.fileName;
		this.path = model.path;
	}

	defaultProps()
	{
		return new Mongoose.Schema({
			fileName: { type: String, required: "fileName is required" },
			path: { type: String, required: "path is required." }
		});
	}

	postIsValid()
	{
		if (!this.fileName)
			return new ReturnModel(201, "Parâmetro 'mongoModel.fileName' não encontrado");

		if (typeof this.fileName != "string")
			return new ReturnModel(201, "Tipo do parametro 'mongoModel.fileName' deve ser 'string'");

		if (!this.path)
			return new ReturnModel(201, "Parâmetro 'mongoModel.path' não encontrado");

		if (typeof this.path != "string")
			return new ReturnModel(201, "Tipo do parametro 'mongoModel.path' deve ser 'string'");

		return new ReturnModel(200, "OK", this);
	}

	putIsValid()
	{
		if (!this._id)
			return new ReturnModel(201, "Parâmetro 'mongoModel._id' não encontrado");

		if (typeof this._id != "string")
			return new ReturnModel(201, "Tipo do parametro 'mongoModel._id' deve ser 'string'");

		return new ReturnModel(200, "OK", this);
	}
}
````

````javascript
import ReturnModel from "../../_returnModel";

import MongoModel from "./models/mongoModel";
import MongoRepository from "../../3.repositories/mongoRepository";

export default class MongoService
{
	async getAll()
	{
		return await new MongoRepository().getAll();
	}

	...

	async addNew(obj)
	{
		let retorno = new MongoModel(obj).postIsValid();

		if (retorno.statusCode == 201) return retorno;

		return await new MongoRepository().addNew(retorno.content);
	}

  ...
}
````

### 3.repositories/

Nesse diretório está presente os _**repositories**_ utilizadas para fazer conexão com o _**Database**_, é obrigatório fazer a herança da classe de conexão pressente em _**4.connections/mongoConnection.js**_. 

Os _**repositories**_ possuem a responsabilidade de conetar no _**Database**_ e tratar seus retornos, isso fazendo uso da funções implementadas pela _**connection**_. Veja:

````javascript
import MongoModel from "../2.services/mongo/models/mongoModel";
import MongoConnection from "../4.connections/mongoConnection";

export default class MongoRepository extends MongoConnection
{
	constructor()
	{
		super("MYDATABASE", "exemplos", new MongoModel().defaultProps());
	}

	async getAll()
	{
		return await super.findAll();
	}

	...

	async addNew(model)
	{
		return await super.save({ fileName: model.fileName, path: model.path });
	}
	 
	...
}
````

### 4.connections/

Nesse diretório está presente a _**connection**_ que utiliza a biblioteca **SEQUELIZE**, essa classe já está configurada para conexão e criação da model de conexão com o _**Database**_. Veja:

````javascript
import Mongoose from "mongoose";

import Parameters from "../_parameters";
import ReturnModel from "../_returnModel";

export default class MongoConnection
{
	constructor(nameDatabase = "MYDATABASE", nameSchema = "exemplos", schema = {})
	{
		this._schema = schema;
		this._nameSchema = nameSchema;
		this._nameDatabase = nameDatabase;
	}

	async connectDb()
	{
		return await Mongoose.connect(`${Parameters.mongoConnection}/${this._nameDatabase}`,
			{
				useNewUrlParser: true,
			},
		);
	}

	async findAll()
	{
		await this.connectDb();

		return await Mongoose
			.model(this._nameSchema, this._schema)
			.find()
			.then(result => {
				return new ReturnModel(200, "OK", result);
			})
			.catch(error => {
				return new ReturnModel(201, error.toString());
			});
	}

	...

	async save(obj = {})
	{
		await this.connectDb();

		let schema = await Mongoose.model(this._nameSchema, this._schema);

		return await new schema(obj)
			.save()
			.then(result => {
				return new ReturnModel(200, "OK", result);
			})
			.catch(error => {
				return new ReturnModel(201, error.toString());
			});
	}
}
````

Exemplo de Chamada e Retorno da API
----

<img src="images/chamada-e-retorno-mongo.png" />
