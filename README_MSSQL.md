# Veja aqui também:

- [API Node + MongoDB](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_MONGODB.md)
- [API Node + Sequelize](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/README_SEQUELIZE.md)

# Read Me For Development With MSSQL

<img src="images/api-icon.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/nodejs-logo.png" width="150" heigth="150" />
<img src="images/plus.png" width="150" heigth="150" />
<img src="images/microsoft-sql-server-logo.png" width="150" heigth="150" />

Nesse repositório está presente uma API funcional que faz conexão ao MongoDB e Sql Server (atráves do MSSQL e Sequelize).

Aqui falaremos sobre a implementação Node.js que faz conexão com Sql Server utilizando a biblioteca _**MSSQL**_, que por sua vez seguiu a seguinte estrutura:

## Estrutura

```javascript
    |
    |_ src
    |   |_ 1.controllers
    |   |   |_ sqlServerController.js
    |   |_ 2.services
    |   |   |_ dtos
    |   |   |   |_ sqlServerDto.js
    |   |   |_ sqlServerService.js
    |   |_ 3.repositories
    |   |   |_ sqlServerRepository.js
    |   |_ 4.connections
    |   |   |_ sqlServerConnection.js
    |		|- customs.js
    |		|- returnDto.js
    |		|- server.js
    |_ app.js
    |_ package-lock.json
    |_ package.json
```

## Explicando a Estrutura

### Arquivos:
- **_customs.js:** Arquivo reponsável por fazer override nos metódos com erro que pertence a uma biblioteca presente na node_modules. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_custom.js)

- **_parameters.js:** Arquivo que contém as variáveis globais que serão acessadas em todo projeto, aconselha-se colocar aqui os caminhos de servidores, usuários e senhas de acesso. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_parameters.js)

- **_returnModel.js:** Model para mensagem padrão de retornos da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_returnModel.js)

- **_routes.js:** Arquivo que chama as rotas presentes nos controllers para acesso as funções da API. [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/_routes.js)

- **app.js:** Arquivo de criação e configuração do servidor que será rodado. (_Obs: Somente esse arquivo em toda API segue as normas do ECMASCRIPT5_). [Veja Mais](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/app.js)

Pastas:
----

### 1.controllers/

Nesse diretório está presente a classe **BaseController** _(responsável por salvar os logs)_ e os demais _**controllers**_ que contém as rotas e suas respectivas funções que as aplicações vão chamar e assim fazer uso da API.  _**Obs:**_ É obrigatório fazer a herança da classe **BaseController** em todos os _controllers_.

Os _**controllers**_ devem ser iniciados e exportados da seguinte forma:

```javascript
import BaseController from "./_baseController";

export default class SqlServerController extends BaseController
{
	constructor()
	{
		super();
	}
}
```

Após sua criação basta adicionar o método _**routes()**_ para criação das rotas das demais funções. É importante fazer uso da função _**super.log()**_ para chamar as funções presente nas classes filhas de **BaseController** para registrar os logs no _**Database**_.

É de responsabilidade dos _**controllers**_ verificar se os parâmetros solicitados pelas funções foram devidamente informados, seja por _query string_ ou pelo _body_ da requisição. [Clique aqui e veja o exemplo completo](https://gitlab.com/consorcio-magalu-new/common_apis/default/blob/master/src/1.controllers/sqlServerController.js).

### 2.services/

Nesse diretório está presente as _**models/**_ utilizadas em toda API, além dos _**services**_ que são responsáveis por verificar, tratar e manipular os dados que serão recebidos e devolvidos na _**request**_.

As _**models/**_ contém os objetos que serão verificados, tratados e manipulados pelas _**services**_. É obrigatório a implementação das funções _**postIsValid()**_ e _**putIsValid()**_ em todas as _**models/**_ para que a regra de verificação dos dados fique somente em um lugar na API e assim facilite sua manutenção. Veja os exemplos de _**model**_ e _**service**_:

````javascript
import ReturnModel from "../../../_returnModel";

export default class SqlServerModel
{
	constructor(model = {})
	{
		this.id = model.id;
		this.var_descricao = model.var_descricao;
		this.flg_ativo = model.flg_ativo;
	}

	postIsValid()
	{
		if (!this.var_descricao)
			return new ReturnModel(201, "Parâmetro 'sqlServerModel.var_descricao' não encontrado");

		if (typeof this.var_descricao != "string")
			return new ReturnModel(201, "Tipo do parametro 'sqlServerModel.var_descricao' deve ser 'string'");

		return new ReturnModel(200, "OK", this);
	}

	putIsValid()
	{
		if (!this.id)
			return new ReturnModel(201, "Parâmetro 'sqlServerModel.id' não encontrado");

		if (typeof this.id != "number")
			return new ReturnModel(201, "Tipo do parametro 'sqlServerModel.id' deve ser 'number'");

		return new ReturnModel(200, "OK", this);
	}
}
````

````javascript
import ReturnModel from "../../_returnModel";

import SqlServerModel from "./models/sqlServerModel";
import SqlServerRepository from "../../3.repositories/sqlServerRepository";

export default class SqlServerService
{
	async getAll()
	{
		return await new SqlServerRepository().getAll();
	}

	...

	async addNew(obj)
	{
		let retorno = await new SqlServerModel(obj).postIsValid();

		if (retorno.statusCode == 201) return retorno;

		return await new SqlServerRepository().addNew(retorno.content);
	}

  ...
}
````

### 3.repositories/

Nesse diretório está presente os _**repositories**_ utilizadas para fazer conexão com o _**Database**_, é obrigatório fazer a herança da classe de conexão pressente em _**4.connections/sqlServerConnection.js**_. 

Os _**repositories**_ possuem a responsabilidade de conetar no _**Database**_ e tratar seus retornos, isso fazendo uso das funções implementadas pela _**connection**_ (**super.query()** e **super.non_query()**). Para realizar a manipulação dos dados no _**Database**_ é preciso fazer uso de _**procedures**_, todos os scripts que foram utilizados nessse desenvolvimento estão na pasta _**./scripts-sql/**_. Veja:

````javascript
import ReturnModel from "../_returnModel";

import SqlServerConnection from "../4.connections/sqlServerConnection";

export default class SqlServerRepository extends SqlServerConnection
{
	constructor()
	{
		super();
	}

	async getAll()
	{
		return await super.query("MYDATABASE", "[dbo].[pr_cns_exemplo]");
	}

	...

	async addNew(model)
	{
		let retorno = await super.non_query("MYDATABASE", "[dbo].[pr_ins_exemplo]", { var_descricao: model.var_descricao });

		switch (retorno.content)
		{
			case 0: return new ReturnModel(200, "OK");
			case 1: return new ReturnModel(201, "Já existe um registro com essa descrição");
			case 2: return new ReturnModel(201, "Falha ao salvar o registro");
			default: return new ReturnModel(201, "Falha ao verificar retorno do banco");
		}
	}
	 
	...
}
````

### 4.connections/

Nesse diretório está presente a _**connection**_ que utiliza a biblioteca **MSSQL**, essa classe já está configurada para conexão e execução das _**procedures**_ no _**Database**_. Veja:

````javascript
import sql from "mssql";

import Parameters from "../_parameters";
import ReturnModel from "../_returnModel";

export default class SqlServerConnection
{
	constructor() { }
	
	async pool(databaseName = "")
	{
		return new sql.ConnectionPool({
			user: Parameters.usuarioServidorDB,
			password: Parameters.senhaServidorDB,
			server: Parameters.caminhoServidorDBTeste,
			database: databaseName,
			options: {
				encrypt: true,
			},
		});
	}

	...
}
````

Exemplo de Chamada e Retorno da API
----

<img src="images/chamada-e-retorno-mssql.png" />
